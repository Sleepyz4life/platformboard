﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour {

	public float speed = 0f;
	public float jumpHeight = 0f;
	public float rotationSpeed= 0f;

	private Rigidbody rigidBody;

	private bool grounded = false;

	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		float xAxis = Input.GetAxis ("Horizontal");
		float zAxis = Input.GetAxis ("Vertical");

		if(xAxis > 0 || xAxis < 0 || zAxis > 0 || zAxis < 0)	
		{
			gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, gameObject.transform.eulerAngles.y + (xAxis * rotationSpeed), 0));
		
			gameObject.transform.Translate(new Vector3(0, 0, zAxis * speed));
		}

		if (Input.GetButtonUp ("Jump") && grounded == true) 
		{
			rigidBody.AddForce (Vector3.up * jumpHeight, ForceMode.Impulse);
			grounded = false; 
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		grounded = true;
        if (collision.gameObject == "deathbox") 
        {
            print("je bent gevallen");
        }
	}
}
