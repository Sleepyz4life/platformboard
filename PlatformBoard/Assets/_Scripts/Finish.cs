﻿using UnityEngine;
using System.Collections;

public class Finish : MonoBehaviour 
{
	private	PlayerManager 	_playerManager	= null; 
	public	PlayerManager 	playerManager
	{
		get
		{
			if (_playerManager == null)
			{
				_playerManager = Hierarchy.GetComponentWithTag<PlayerManager>("PlayerManager");
			}
			return _playerManager;
		}		
	} 

	public string button,
				  textElement;

	public GUIStyle style;
	private GUIStyle scaled;

	private bool finished = false;

	private void OnGUI()
	{
		scaled = GUIMaster.ResolutionGUIStyle (style);

		if (finished) 
		{
			GUI.Label (GUIMaster.GetElementRect (textElement), "The winner is: Player " + playerManager.Winner ().ToString (), scaled);

			if (GUI.Button (GUIMaster.GetElementRect (button), "Back to Menu", scaled)) {
				Application.LoadLevel ("Nature_Menu");
			}
		}
	}

	void OnTriggerEnter(Collider other) 
	{
		finished = true;
	}
}
